# Python library project sample

This project sample shows the usage of _to be continuous_ templates:

* Python
* SonarQube (from [sonarcloud.io](https://sonarcloud.io/))

The project publish a basic library in Python (3.9) on AWS Lambda, and implements automated accetance tests with Postman.

## Python template features

This project uses the following features from the GitLab CI Python template:

* Uses the `pyproject.toml` build specs file with [Poetry](https://python-poetry.org/) as build backend,
* Enables the [pytest](https://docs.pytest.org/) unit test framework by declaring the `$PYTEST_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables [pylint](https://pylint.pycqa.org/) by declaring the `$PYLINT_ENABLED` in the `.gitlab-ci.yml` variables,
* Enables the [Bandit](https://pypi.org/project/bandit/) SAST analysis job by declaring the `$BANDIT_ENABLED` and skips the `B311`
  test by overriding `$BANDIT_ARGS` in the `.gitlab-ci.yml` variables.

The Python template also enforces:

* [test report integration](https://docs.gitlab.com/ee/ci/testing/unit_test_reports.html),
* and [code coverage integration](https://docs.gitlab.com/ee/user/project/pipelines/settings.html#test-coverage-report-badge).

## SonarQube template features

This project uses the following features from the SonarQube template:

* Defines the `SONAR_HOST_URL` (SonarQube server host),
* Defines `organization` & `projectKey` from [sonarcloud.io](https://sonarcloud.io/) in `sonar-project.properties`,
* Defines :lock: `$SONAR_TOKEN` as secret CI/CD variable,
* Uses the `sonar-project.properties` to specify project specific configuration:
    * source and test folders,
    * code coverage report (from `pytest`),
    * unit test reports (from `pytest`).
